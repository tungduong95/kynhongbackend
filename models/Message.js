const mongoose = require("mongoose");

const MessageSchema = mongoose.Schema(
  {
    content: {
      text: { type: String, required: true },
    },

    user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },

    sender: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    read: { type: Date },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Messagechema", MessageSchema);
