const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const User = require("../models/User");
const jwtkey = require("../config");

module.exports = (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return res.status(403).json({message:"Authorization failed!"});
  }

  const token = authorization.replace("Bearer ", "");
  jwt.verify(token, jwtkey, async (err, payload) => {
    if (err) {
      return res.status(403).json({ error: "Authentication failed!" });
    }
    const userId = payload;
    const user = await User.findById(payload.userId);
    req.user =user;
    next();
  });
};
