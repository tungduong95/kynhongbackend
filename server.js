const express = require('express');
const formidable = require('express-formidable');
const http = require('http');
const io = require('socket.io');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const { json } = require('body-parser');
const cors = require('cors');
const userRoute = require('./routes/userRoute');
const loginRoute = require('./routes/loginRoute');
const contactRoute = require('./routes/contactRoute');
const messageRoute =require('./routes/messageRoute');
const token =require('./middleware/token');
const app = express();
const server = http.createServer(app);
// app.use(formidable());

app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

//app.use('/public', express.static(__dirname + '/public'));

//app.set('view engine', 'ejs');

const socketio = io(server);
const socketId = '';
const users = [];

socketio.on('connection', (socket) => {
  console.log('user connected', socket.id);
  socketId = socket.id;
});

mongoose.connect('mongodb+srv://tuzdu:tuzdu12021996@cluster0-917ka.mongodb.net/mami?retryWrites=true&w=majority',
    {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true
    }
  )
  .then(() => console.log('MongoDB connected'))
  .catch((err) => console.log(err));
app.use(cors());
app.use('/user', userRoute);
app.use('/login',loginRoute);
app.use('/contact', contactRoute);
app.use('/message',messageRoute);
app.use('/',(req,res)=>{
  res.status(200).json({message:"Welcome! But we don't have anything to show here, just some backend stuff"})
})
const PORT = process.env.PORT||3000;
server.listen(PORT, () => {
  console.log(`Server is listening`);
});
