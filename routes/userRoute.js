const express = require('express');
const User = require('../models/User');
const bcrypt = require('bcrypt');
const tokenVerify = require('../middleware/token');

const router = express.Router();

router.get('/', (req, res) => {    //Liet ke danh sach user
  User.find((err,doc)=>{
    if(err) res.status(400).json({message:err})
    else res.status(200).json({message:doc})
  });
});

router.get('/friends/:id',(req,res)=>{//Liet ke danh sach ban be cua user
  User.findById(req.params.id,(err,doc)=>{
    if(err) res.status(400).json({message:err})
    else res.status(200).json({message:doc.friends})
  })
});

router.post('/friends/add/:id',(req,res)=>{ //them ban be
  User.countDocuments({'friends.user':req.params.id},(err,count)=>{
    if(count>0){
      res.status(400).json('User already friend!')
    }
    else{
    const friend ={
      user: req.params.id,
      status:'3'
    }
  
    User.findByIdAndUpdate(req.body.id,{$push:{friends:friend}}
      ,{new:true},(err,doc)=>{
      if(err) res.status(400).json({message:err})
      else res.status(200).json({message:doc})
    }  )
  }})
});

router.post('/', (req, res) => {//them user
    bcrypt.hash(req.body.password, 10, (err, hash) => {
      const user = new User({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        
        password: hash,
      });
      user
        .save()
        .then((item) => {
          res.status(200).json(user);
        })
        .catch((err) => {
          res.status(400).json({ err });
        });
    });
});

router.delete('/friends/remove/:id',(req,res)=>{//xoa user
  User.findByIdAndRemove(req.params.id,(err,doc)=>{
    if(err){
      res.status(400).json({ message: 'Cant delete user' });
    }
    else{
      res.status(200).json(doc);
    }
  })
});

module.exports = router;
